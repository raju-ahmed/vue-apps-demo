import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import blabla from '@/modules/peoples/route'
Vue.use(Router)

const baseRoute = [
  {
    path: '/',
    name: 'HelloWorld',
    component: HelloWorld
  }
]
var route = baseRoute.concat(blabla)
export default new Router({
  mode: 'history',
  routes: route
})
