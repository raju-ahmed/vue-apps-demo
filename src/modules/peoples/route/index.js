import People from '../views/People'
import PeopleCreate from '../views/PeopleCreate'
import PeopleDetail from '../views/PeopleDetail'
import PeopleWelcome from '../views/PeopleWelcome'

// const peopleRoute =

export default [
  {
    path: '/people',
    name: 'people',
    component: People,
    title: 'People',
    redirect: {name: 'people-welcome'},
    children: [
      {
        path: 'welcome',
        name: 'people-welcome',
        component: PeopleWelcome
      },
      {
        path: 'detail',
        name: 'people-detail',
        component: PeopleDetail
      },
      {
        path: 'create',
        name: 'people-create',
        component: PeopleCreate
      }
    ]
  }
]
